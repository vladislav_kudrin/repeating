package tasks.task9;

import java.util.Scanner;

/**
 * A class checks whether a number is a palindrome or not.
 *
 *
 *
 * @author nxfh
 */
public class NumberPalindrome {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a number: ");

        System.out.println("\nThis number is " + ((checkPalindrome(input.nextInt())) ? "a palindrome." : "not a palindrome."));
    }

    /**
     * A method checks whether the number is a palindrome or not.
     *
     * @param number the number.
     * @return a result.
     */
    private static boolean checkPalindrome(int number) {
        int reversedNumber = 0, remainder;

        for(int temporaryNumber = number; temporaryNumber != 0; temporaryNumber /= 10) {
            remainder = temporaryNumber % 10;
            reversedNumber = reversedNumber * 10 + remainder;
        }

        return reversedNumber == number;
    }
}