package tasks.task12;

import java.util.Arrays;

/**
 * A class converts a multidimensional array to one dimensional array.
 *
 *
 *
 * @author nxfh
 */
public class ArraysConverter {
    public static void main(String[] args) {
        int[][] multidimensionalArray = new int[3][3];
        int[] oneDimensionalArray = new int[9];

        arrayFiller(multidimensionalArray);

        System.out.println("Multidimensional array: ");
        showMultiArray(multidimensionalArray);

        convertArrays(multidimensionalArray, oneDimensionalArray);

        System.out.println("One dimensional array: " + Arrays.toString(oneDimensionalArray));
    }

    /**
     * A method fills an array with random numbers from -5 to 5.
     *
     * @param multidimensionalArray the empty array.
     */
    private static void arrayFiller(int[][] multidimensionalArray) {
        for(int line = 0; line < multidimensionalArray.length; line++) {
            for(int column = 0; column < multidimensionalArray[line].length; column++) {
                multidimensionalArray[line][column] = (int) (Math.random() * 12 - 6);
            }
        }
    }

    /**
     * A method shows the multidimensional array.
     *
     * @param multidimensionalArray the multidimensional array.
     */
    private static void showMultiArray(int[][] multidimensionalArray) {
        for(int line = 0; line < multidimensionalArray.length; line++) {
            for(int column = 0; column < multidimensionalArray[line].length; column++) {
                System.out.print(multidimensionalArray[line][column] + " ");
            }

            System.out.print("\n");
        }

        System.out.print("\n");
    }

    /**
     * A method converts the multidimensional array to one dimensional array.
     *
     * @param multidimensionalArray the multidimensional array.
     * @param oneDimensionalArray the one dimensional array.
     */
    private static void convertArrays(int[][] multidimensionalArray, int[] oneDimensionalArray) {
        int index = 0;

        for (int line = 0; line < multidimensionalArray.length; line++) {
            for (int column = 0; column < multidimensionalArray[line].length; column++) {
                oneDimensionalArray[index++] = multidimensionalArray[line][column];
            }
        }
    }
}