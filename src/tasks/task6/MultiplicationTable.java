package tasks.task6;

import java.util.Scanner;

/**
 * A class shows a multiplication table of entered number.
 *
 *
 *
 * @author nxfh
 */
public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a number that multiplication table you wanna see: ");
        showMultiplicationTable(input.nextInt());
    }

    /**
     * A method calculates and shows the multiplication table of entered number.
     *
     * @param enteredNumber entered number.
     */
    private static void showMultiplicationTable(int enteredNumber) {
        System.out.print("\n");

        for(int number = 1; number <= 10; number++) {
            System.out.println(enteredNumber + " * " + number + " = " + (enteredNumber * number));
        }
    }
}