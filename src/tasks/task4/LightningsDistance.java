package tasks.task4;

import java.util.Scanner;

/**
 * A class calculates a lightning's distance.
 *
 *
 *
 * @author nxfh
 */
public class LightningsDistance {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double interval;

        System.out.print("Please, enter an interval as seconds: ");
        interval = input.nextDouble();

        System.out.printf("\nLightning's destination = %5.2f km.", calculateLightningsDistance(interval));
    }

    /**
     * A method calculates the lightning's distance.
     *
     * @param interval the interval.
     * @return a result.
     */
    private static double calculateLightningsDistance(double interval) {
        return 1234.8 / 3600 * interval;
    }
}