package tasks.task11;

import java.util.Scanner;

/**
 * A class converts days into hours, minutes and seconds.
 *
 *
 *
 * @author nxfh
 */
public class DaysConverter {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a number of days: ");
        convertDays(input.nextInt());
    }

    /**
     * A method converts days into hours, minutes and seconds.
     *
     * @param daysNumber the number of days.
     */
    private static void convertDays(int daysNumber) {
        System.out.println("\nHours: " + (daysNumber * 24) + "\nMinutes: " + (daysNumber * 1440) + "\nSeconds: " + (daysNumber * 86400));
    }
}