package tasks.task1;

import java.util.Scanner;

/**
 * A class calculates a spaces number.
 *
 *
 *
 * @author nxfh
 */
public class SpacesNumber {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a text: ");
        String[] text = input.nextLine().split("\\.");

        System.out.println("\nFirst tense: " + text[0] + ".");
        System.out.println("Spaces number in the first tense: " + calculateSpacesNumber(text[0]));
    }

    /**
     * A method calculates the spaces number.
     *
     * @param text the text.
     * @return the spaces number.
     */
    private static int calculateSpacesNumber(String text) {
        return text.length() - text.replaceAll(" ", "").length();
    }
}