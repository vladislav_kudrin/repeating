package tasks.task7;

import java.util.Scanner;

/**
 * A class checks a number's type.
 *
 *
 *
 * @author nxfh
 */
public class TypeChecker {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a number: ");

        System.out.println("\nThis number is " + ((checkType(input.nextDouble()) ? "integer." : "not integer.")));
    }

    /**
     * A method checks the number's type.
     *
     * @param number the number.
     * @return a result.
     */
    private static boolean checkType(double number) {
        return number % 1 == 0;
    }
}