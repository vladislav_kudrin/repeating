package tasks.task13;

import java.util.Scanner;

/**
 * A class checks an entered character.
 *
 *
 *
 * @author nxfh
 */
public class CheckCharacter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a character: ");
        checkCharacter(input.next());
    }

    /**
     * A method checks the entered character.
     *
     * @param characters the entered character.
     */
    private static void checkCharacter(String characters) {
        char character = characters.charAt(0);
        String punctuationSigns = ".,:;?!\"\'[]{}()-";

        if(Character.isDigit(character)) {
            System.out.println("\nThis character is a digit.");
        }
        else if(Character.isLetter(character)) {
            System.out.println("\nThis character is a letter.");
        }
        else if(punctuationSigns.contains(characters)){
            System.out.println("\nThis character is a punctuation sign.");
        }
    }
}