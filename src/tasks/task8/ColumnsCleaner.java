package tasks.task8;

import java.util.Scanner;

/**
 * A class clears a chosen column of matrix.
 *
 *
 *
 * @author nxfh
 */
public class ColumnsCleaner {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[][] matrix = {{1, 2}, {3, 4}, {5, 6}};

        System.out.println("Matrix:");
        showMatrix(matrix);

        System.out.print("Please, enter a number of column that you wanna clear: ");
        clearColumn(matrix, input.nextInt() - 1);

        System.out.println("\nA new matrix:");
        showMatrix(matrix);
    }

    /**
     * A method shows the matrix.
     *
     * @param matrix the matrix.
     */
    private static void showMatrix(int[][] matrix) {
        for(int line = 0; line < matrix.length; line++) {
            for(int column = 0; column < matrix[line].length; column++) {
                System.out.print(matrix[line][column] + " ");
            }

            System.out.print("\n");
        }

        System.out.print("\n");
    }

    /**
     * A method clears the chosen column of the matrix.
     *
     * @param matrix the matrix.
     * @param number the column's number.
     */
    private static void clearColumn(int[][] matrix, int number) {
        for(int line = 0; line < matrix.length; line++) {
            matrix[line][number] = 0;
        }
    }
}