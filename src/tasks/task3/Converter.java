package tasks.task3;

import java.util.Scanner;

/**
 * A class converts RUB to EUR.
 *
 *
 *
 * @author nxfh
 */
public class Converter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double rub, eur;

        System.out.print("Please, enter a number of rubles: ");
        rub = input.nextDouble();
        System.out.print("Please, enter a number of euros: ");
        eur = input.nextDouble();

        System.out.printf("\n%5.2f RUB = %5.2f EUR.",rub, convert(rub, eur));
    }

    /**
     * A method converts RUB to EUR.
     *
     * @param rub number of rubles.
     * @param eur exchange rate.
     * @return a result.
     */
    private static double convert(double rub, double eur) {
        return rub * eur;
    }
}