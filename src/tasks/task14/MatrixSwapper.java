package tasks.task14;

/**
 * A class swaps matrix lines and columns.
 *
 *
 *
 * @author nxfh
 */
public class MatrixSwapper {
    public static void main(String[] args) {
        int[][] matrix = {{1,2}, {3,4}};

        System.out.println("A matrix:");
        showMatrix(matrix);

        swapMatrix(matrix);

        System.out.println("A swapped matrix:");
        showMatrix(matrix);
    }

    /**
     * A method shows the matrix.
     *
     * @param matrix the matrix.
     */
    private static void showMatrix(int[][] matrix) {
        for(int line = 0; line < matrix.length; line++) {
            for(int column = 0; column < matrix[line].length; column++) {
                System.out.print(matrix[line][column] + " ");
            }

            System.out.print("\n");
        }

        System.out.print("\n");
    }

    /**
     * A method swaps matrix lines and columns.
     *
     * @param matrix the matrix.
     */
    private static void swapMatrix(int[][] matrix) {
        int changer;

        for(int line = 1; line < matrix.length; line++) {
            for (int column = 0; column < matrix[line].length; column++) {
                changer = matrix[line][column];
                matrix[line][column] = matrix[column][line];
                matrix[column][line] = changer;
            }
        }
    }
}