package tasks.task5;

/**
 * A class shows prime numbers from 2 to 100.
 *
 *
 *
 * @author nxfh
 */
public class PrimeNumbers {
    public static void main(String[] args) {
        showPrimeNumbers();
    }

    /**
     * A method calculates and shows prime numbers.
     */
    private static void showPrimeNumbers() {
        int counter = 0;

        System.out.print("Prime numbers from 2 to 100:\n2");

        for(int number = 3; number <= 100; number++) {
            for(int index = 2; index <= number / 2; index++) {
                counter = (number % index == 0) ? counter + 1 : counter;
            }

            if(counter == 0) {
                System.out.print(", " + number);
            }
            else {
                counter = 0;
            }
        }
    }
}