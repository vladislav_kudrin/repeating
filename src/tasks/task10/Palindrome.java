package tasks.task10;

import java.util.Scanner;

/**
 * A class checks whether a string is a palindrome or not.
 *
 *
 *
 * @author nxfh
 */
public class Palindrome {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a phrase: ");
        System.out.println("\nThis phrase is "+ ((checkPalindrome(input.nextLine())) ? "a palindrome." : "not a palindrome."));
    }

    /**
     * A method checks whether the string is a palindrome or not.
     *
     * @param phrase the string.
     * @return a result.
     */
    private static boolean checkPalindrome(String phrase) {
        phrase = phrase.replaceAll("[- ,.:;(){}\'\"!?]", "").toLowerCase();
        String reversedPhrase = new StringBuffer(phrase).reverse().toString();

        return reversedPhrase.equals(phrase);
    }
}