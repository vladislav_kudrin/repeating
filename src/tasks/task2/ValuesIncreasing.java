package tasks.task2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * A class increases a value of specified array's element.
 *
 *
 *
 * @author nxfh
 */
public class ValuesIncreasing {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double[] array = {15, 40, 32, 81, 16};

        System.out.println("Array: " + Arrays.toString(array));
        System.out.print("\nPlease, enter a number of array's element (from 1 to 5) to increase its value by 10%: ");

        increaseValue(array, input.nextInt() - 1);

        System.out.println("\nNew array: " + Arrays.toString(array));
    }

    /**
     * A method increases the value of specified array's element.
     *
     * @param array the array.
     * @param index an index of specified array's element.
     */
    private static void increaseValue(double[] array, int index) {
        array[index] = array[index] + array[index] * 0.1;
    }
}